var fs = require('fs');
var Stripe = require('stripe');

module.exports = function(wagner) {

  wagner.factory('Config', function() {
    return JSON.parse(fs.readFileSync('./config.json').toString());
  });

  wagner.invoke(function(Config) {
    // property to get the Stripe API key.
    wagner.factory('Stripe', function() {
      return Stripe(Config.stripeKey);
    });
  });

  wagner.invoke(function(Config) {
    var fx = require('./fx');
    wagner.factory('fx', fx);
  });
};
